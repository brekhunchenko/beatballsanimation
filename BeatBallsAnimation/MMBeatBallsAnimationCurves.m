//
//  MMBeatBallsAnimationCurves.m
//  BeatBallsAnimation
//
//  Created by Yaroslav on 8/30/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMBeatBallsAnimationCurves.h"

double bezier(double time, double A, double B, double C) {
    return time * (C + time * (B + time * A)); //A t^3 + B t^2 + C t
}

double bezier_der(double time, double A, double B, double C) {
    return C + time * (2 * B + time * 3 * A); //3 A t^2 + 2 B t + C
}

double xForTime(double time, double ctx1, double ctx2) {
    double x = time, z;
    
    double C = 3 * ctx1;
    double B = 3 * (ctx2 - ctx1) - C;
    double A = 1 - C - B;
    
    int i = 0;
    while (i < 5) {
        z = bezier(x, A, B, C) - time;
        if (fabs(z) < 0.001) break;
        
        x = x - z / bezier_der(x, A, B, C);
        i++;
    }
    
    return x;
}

const double (^MMAnimationCurveAnimationBezierEvaluator)(double, CGPoint, CGPoint) = ^(double time, CGPoint ct1, CGPoint ct2) {
    double Cy = 3 * ct1.y;
    double By = 3 * (ct2.y - ct1.y) - Cy;
    double Ay = 1 - Cy - By;
    
    return bezier(xForTime(time, ct1.x, ct2.x), Ay, By, Cy);
};

const MMAnimationCurveTimeBlock MMAnimationCurveTimeBlockLinear = ^(double time) {
    return time;
};

const MMAnimationCurveTimeBlock MMAnimationCurveTimeBlockQuadraticIn = ^(double time) {
    return pow(time, 2);
};

const MMAnimationCurveTimeBlock MMAnimationCurveTimeBlockQuadraticOut = ^(double time) {
    return 1 - pow(1 - time, 2);
};

const MMAnimationCurveTimeBlock MMAnimationCurveTimeBlockQuadraticInOut = ^(double time) {
    time *= 2.0;
    if (time < 1) return MMAnimationCurveTimeBlockQuadraticIn(time) / 2.0;
    time--;
    return (1 + MMAnimationCurveTimeBlockQuadraticOut(time)) / 2.0;
};

const MMAnimationCurveTimeBlock MMAnimationCurveTimeBlockAppleIn = ^(double time) {
    CGPoint ct1 = CGPointMake(0.42, 0.0), ct2 = CGPointMake(1.0, 1.0);
    return MMAnimationCurveAnimationBezierEvaluator(time, ct1, ct2);
};

const MMAnimationCurveTimeBlock MMAnimationCurveTimeBlockAppleOut = ^(double time) {
    CGPoint ct1 = CGPointMake(0.0, 0.0), ct2 = CGPointMake(0.58, 1.0);
    return MMAnimationCurveAnimationBezierEvaluator(time, ct1, ct2);
};

const MMAnimationCurveTimeBlock MMAnimationCurveTimeBlockAppleInOut = ^(double time) {
    CGPoint ct1 = CGPointMake(0.42, 0.0), ct2 = CGPointMake(0.58, 1.0);
    return MMAnimationCurveAnimationBezierEvaluator(time, ct1, ct2);
};

const MMAnimationCurveTimeBlock MMAnimationCurveTimeBlockExpoIn = ^(double time) {
    if (time == 0.0) {
        return 0.0;
    }
    return pow(2, 10 * (time - 1));
};

const MMAnimationCurveTimeBlock MMAnimationCurveTimeBlockExpoOut = ^(double time) {
    if (time == 1.0) {
        return 1.0;
    }
    return -pow(2, -10 * time) + 1;
};

const MMAnimationCurveTimeBlock MMAnimationCurveTimeBlockExpoInOut = ^(double time) {
    if (time == 0) {
        return 0.0;
    }
    if (time == 1) {
        return 1.0;
    }
    time *= 2;
    if (time < 1) return 0.5 * pow(2, 10 * (time - 1));
    --time;
    return 0.5 * (-pow(2, -10 * time) + 2);
};

const MMAnimationCurveTimeBlock MMAnimationCurveTimeBlockCircularIn = ^(double time) {
    return 1 - sqrt(1 - time * time);
};

const MMAnimationCurveTimeBlock MMAnimationCurveTimeBlockCircularOut = ^(double time) {
    return sqrt(1 - pow(time - 1, 2));
};

const MMAnimationCurveTimeBlock MMAnimationCurveTimeBlockCircularInOut = ^(double time) {
    time *= 2;
    if (time < 1) return -0.5 * (sqrt(1 - pow(time, 2)) - 1);
    time -= 2;
    return 0.5 * (sqrt(1 - pow(time, 2)) + 1);
};

const MMAnimationCurveTimeBlock MMAnimationCurveTimeBlockBackIn = ^(double time) {
    CGPoint ct1 = CGPointMake(0.6, -0.28), ct2 = CGPointMake(0.735, 0.045);
    return MMAnimationCurveAnimationBezierEvaluator(time, ct1, ct2);
};

const MMAnimationCurveTimeBlock MMAnimationCurveTimeBlockBackOut = ^(double time) {
    CGPoint ct1 = CGPointMake(0.175, 0.885), ct2 = CGPointMake(0.32, 1.275);
    return MMAnimationCurveAnimationBezierEvaluator(time, ct1, ct2);
};

const MMAnimationCurveTimeBlock MMAnimationCurveTimeBlockBackInOut = ^(double time) {
    CGPoint ct1 = CGPointMake(0.68, -0.55), ct2 = CGPointMake(0.265, 1.55);
    return MMAnimationCurveAnimationBezierEvaluator(time, ct1, ct2);
};
