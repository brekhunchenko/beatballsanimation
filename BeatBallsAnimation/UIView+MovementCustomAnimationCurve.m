//
//  UIView+MovementCustomAnimationCurve.m
//  BeatBallsAnimation
//
//  Created by Yaroslav on 8/30/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "UIView+MovementCustomAnimationCurve.h"

@implementation UIView (MovementCustomAnimationCurve)

+ (void)animateMovementFrom:(CGPoint)p0
                         to:(CGPoint)p1
                       view:(UIView *)view
             animationCurve:(MMAnimationCurveTimeBlock)animationCurve
                   duration:(NSTimeInterval)duration
                 completion:(void (^)(BOOL))completionBlock {

    NSInteger numSteps = 101;
    void (^addKeyFrames)(void) = ^{
        double time = 0.0, prevTime = 0.0;
        double timeStep = 1.0/(double)(numSteps - 1);
        for (NSUInteger i = 0; i < numSteps; i++) {
            prevTime = time;
            
            [UIView addKeyframeWithRelativeStartTime:prevTime
                                    relativeDuration:i == 0 ? 0 : timeStep
                                          animations:^{
                                              CGFloat progress = animationCurve(time);
                                              CGPoint updatedPoint = CGPointMake([self _animationLerpForProgress:progress fromValue:p0.x to:p1.x],
                                                                                 [self _animationLerpForProgress:progress fromValue:p0.y to:p1.y]);
                                              [view setValue:[NSValue valueWithCGPoint:updatedPoint] forKey:@"center"];
                                          }];
            
            time = MIN(1, MAX(0, time + timeStep));
        }
    };
    
    UIViewKeyframeAnimationOptions options = UIViewKeyframeAnimationOptionCalculationModeLinear;
    options = options | UIViewAnimationOptionCurveLinear;
    
    [UIView animateKeyframesWithDuration:duration
                                   delay:0.0f
                                 options:options
                              animations:addKeyFrames
                              completion:completionBlock];
}

#pragma mark - Helpers

+ (CGFloat)_animationLerpForProgress:(CGFloat)progress fromValue:(CGFloat)fromValue to:(CGFloat)toValue {
    return fromValue + (toValue - fromValue)*progress;
}

@end
