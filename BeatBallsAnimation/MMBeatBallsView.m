//
//  MMBeatBallsView.m
//  BeatBallsAnimation
//
//  Created by Yaroslav on 8/30/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMBeatBallsView.h"
#import "UIView+MovementCustomAnimationCurve.h"

@interface MMBeatBallsView()

@property (nonatomic, strong) NSArray<UIImageView *> *ballsViews;
@property (nonatomic, strong) UIView *ballsContainerView;

@end

@implementation MMBeatBallsView

#pragma mark - Initializers

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self _setupBallsContainerView];
    
    _ballsMovementDistance = 100.0f;
    _ballAnimationTimePeriod = 1.0f;
    _animationCurve = MMAnimationCurveTimeBlockAppleInOut;
    
    _distanceBetweenBalls = 30.0f;
    _ballSize = 44.0f;
    _numberOfBalls = 3;
    [self _setupUI];
}

#pragma mark - Setup

- (void)_setupBallsContainerView {
    _ballsContainerView = [[UIView alloc] initWithFrame:CGRectZero];
    _ballsContainerView.backgroundColor = [UIColor clearColor];
    [self addSubview:_ballsContainerView];
}

- (void)_setupUI {
    [_ballsViews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    NSMutableArray* ballsViewsMutable = [NSMutableArray new];
    for (int i = 0; i < _numberOfBalls; i++) {
        UIImageView* ballImageView = [[UIImageView alloc] initWithFrame:CGRectMake(i*(_ballSize + _distanceBetweenBalls),
                                                                                   0.0f,
                                                                                   _ballSize,
                                                                                   _ballSize)];
        ballImageView.layer.cornerRadius = ballImageView.frame.size.width/2.0f;
        ballImageView.layer.cornerRadius = ballImageView.frame.size.height/2.0f;
        ballImageView.backgroundColor = [UIColor colorWithRed:118.0f/255.0f
                                                        green:244.0f/255.0f
                                                         blue:156.0f/255.0f
                                                        alpha:1.0f];
        [_ballsContainerView addSubview:ballImageView];
        
        [ballsViewsMutable addObject:ballImageView];
    }
    _ballsViews = [NSArray arrayWithArray:ballsViewsMutable];
    
    _ballsContainerView.frame = CGRectMake(0.0f,
                                           0.0f,
                                           (_numberOfBalls - 1)*(_ballSize + _distanceBetweenBalls) + _ballSize,
                                           _ballsMovementDistance + _ballSize);
    _ballsContainerView.center = CGPointMake(self.bounds.size.width/2.0f,
                                             self.bounds.size.height/2.0f);
}

#pragma mark - Custom Setters & Getters

- (void)setNumberOfBalls:(NSUInteger)numberOfBalls {
    if (_numberOfBalls != numberOfBalls) {
        _numberOfBalls = numberOfBalls;
        
        [self _setupUI];
    }
}

- (void)setBallSize:(CGFloat)circleSize {
    if (_ballSize != circleSize) {
        _ballSize = circleSize;
        
        [self _setupUI];
    }
}

- (void)setDistanceBetweenCircles:(CGFloat)distanceBetweenCircles {
    if (_distanceBetweenBalls != distanceBetweenCircles) {
        _distanceBetweenBalls = distanceBetweenCircles;
        
        [self _setupUI];
    }
}

#pragma mark - Class Methods

- (void)showMovementAnimationForBallAtIndex:(NSInteger)ballIndex {
    if (ballIndex < _numberOfBalls) {
        UIImageView* ballImageView = _ballsViews[ballIndex];
        [ballImageView.layer removeAllAnimations];
        [UIView animateMovementFrom:ballImageView.center
                                 to:CGPointMake(ballImageView.center.x,
                                                _ballsContainerView.frame.size.height - ballImageView.frame.size.height/2.0f)
                             view:ballImageView
                     animationCurve:_animationCurve
                           duration:(_ballAnimationTimePeriod/2.0f)
                         completion:^(BOOL finished) {
                             
             [UIView animateMovementFrom:ballImageView.center
                                      to:CGPointMake(ballImageView.center.x,
                                                     ballImageView.frame.size.height/2.0f)
                                  view:ballImageView
                          animationCurve:_animationCurve
                                duration:(_ballAnimationTimePeriod/2.0f)
                              completion:nil];
         }];
    }
}

@end
