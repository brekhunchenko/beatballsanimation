//
//  MMBeatBallsView.h
//  BeatBallsAnimation
//
//  Created by Yaroslav on 8/30/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMBeatBallsAnimationCurves.h"

@interface MMBeatBallsView : UIView

/* Balls UI */
@property (nonatomic, assign) CGFloat ballSize;
@property (nonatomic, assign) CGFloat distanceBetweenBalls;
@property (nonatomic, assign) NSUInteger numberOfBalls;

/* Animation */
@property (nonatomic, assign) CGFloat ballsMovementDistance;
@property (nonatomic, assign) CGFloat ballAnimationTimePeriod;
@property (nonatomic, strong) MMAnimationCurveTimeBlock animationCurve;
- (void)showMovementAnimationForBallAtIndex:(NSInteger)ballIndex;

@end
