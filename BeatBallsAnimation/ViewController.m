//
//  ViewController.m
//  BeatBallsAnimation
//
//  Created by Yaroslav on 8/30/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "ViewController.h"
#import "MMBeatBallsView.h"

typedef NS_ENUM(NSInteger, MMAnimationCurve) {
    MMAnimationCurveAppleIn,
    MMAnimationCurveAppleOut,
    MMAnimationCurveAppleInOut,
    MMAnimationCurveQuadraticIn,
    MMAnimationCurveQuadraticOut,
    MMAnimationCurveQuadraticInOut,
    MMAnimationCurveExpoIn,
    MMAnimationCurveExpoOut,
    MMAnimationCurveExpoInOut,
    MMAnimationCurveCircularIn,
    MMAnimationCurveCircularOut,
    MMAnimationCurveCircularInOut,
    MMAnimationCurveBackIn,
    MMAnimationCurveBackOut,
    MMAnimationCurveBackInOut,
};

@interface ViewController () < UITableViewDelegate, UITableViewDataSource >

@property (weak, nonatomic) IBOutlet MMBeatBallsView *beatBallsView;
@property (weak, nonatomic) IBOutlet UISlider *numberOfBallsSlider;
@property (weak, nonatomic) IBOutlet UISlider *animationTimePeriodSlider;
@property (weak, nonatomic) IBOutlet UILabel *numberOfBallsLabel;
@property (weak, nonatomic) IBOutlet UILabel *animaitonTimePeriodLabel;
@property (weak, nonatomic) IBOutlet UITableView *animationCurvesTableView;
@property (nonatomic, strong) NSArray* listOfAnimationCurves;

@end

@implementation ViewController

#pragma mark - UIViewController LifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self _setupBeatBallsView];
    
    _listOfAnimationCurves = @[@(MMAnimationCurveAppleIn),
                               @(MMAnimationCurveAppleOut),
                               @(MMAnimationCurveAppleInOut),
                               @(MMAnimationCurveQuadraticIn),
                               @(MMAnimationCurveQuadraticOut),
                               @(MMAnimationCurveQuadraticInOut),
                               @(MMAnimationCurveExpoIn),
                               @(MMAnimationCurveExpoOut),
                               @(MMAnimationCurveExpoInOut),
                               @(MMAnimationCurveCircularIn),
                               @(MMAnimationCurveCircularOut),
                               @(MMAnimationCurveCircularInOut),
                               @(MMAnimationCurveBackIn),
                               @(MMAnimationCurveBackOut),
                               @(MMAnimationCurveBackInOut)
                              ];
}

#pragma mark - Setup

- (void)_setupBeatBallsView {
    _beatBallsView.backgroundColor = [UIColor clearColor];
    _beatBallsView.ballSize = 22.0f;
    _beatBallsView.numberOfBalls = 3;
}

#pragma mark - Actions

- (IBAction)showAnimation:(id)sender {
    [self _showBallsAnimation];
}

- (IBAction)numberOfBallsSliderValueChanged:(UISlider *)sender {
    NSInteger numberOfBalls = (int)[sender value];
    [_beatBallsView setNumberOfBalls:numberOfBalls];
    
    _numberOfBallsLabel.text = [NSString stringWithFormat:@"Number of balls: %d", (int)numberOfBalls];
}

- (IBAction)animaitonTimePeriodSliderValueChanged:(UISlider *)sender {
    CGFloat animationTimePeriod = [sender value];
    [_beatBallsView setBallAnimationTimePeriod:animationTimePeriod];
    
    _animaitonTimePeriodLabel.text = [NSString stringWithFormat:@"Animaiton time period: %.1f sec", animationTimePeriod];
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([UITableViewCell class])];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    switch (indexPath.row) {
        case MMAnimationCurveAppleIn: {
            cell.textLabel.text = @"MMAnimationCurveAppleIn";
        }
            break;
        case MMAnimationCurveAppleOut: {
            cell.textLabel.text = @"MMAnimationCurveAppleOut";
        }
            break;
        case MMAnimationCurveAppleInOut: {
            cell.textLabel.text = @"MMAnimationCurveAppleInOut";
        }
            break;
        case MMAnimationCurveQuadraticIn: {
            cell.textLabel.text = @"MMAnimationCurveQuadraticIn";
        }
            break;
        case MMAnimationCurveQuadraticOut: {
            cell.textLabel.text = @"MMAnimationCurveQuadraticOut";
        }
            break;
        case MMAnimationCurveQuadraticInOut: {
            cell.textLabel.text = @"MMAnimationCurveQuadraticInOut";
        }
            break;
        case MMAnimationCurveExpoIn: {
            cell.textLabel.text = @"MMAnimationCurveExpoIn";
        }
            break;
        case MMAnimationCurveExpoOut: {
            cell.textLabel.text = @"MMAnimationCurveExpoOut";
        }
            break;
        case MMAnimationCurveExpoInOut: {
            cell.textLabel.text = @"MMAnimationCurveExpoInOut";
        }
            break;
        case MMAnimationCurveCircularIn: {
            cell.textLabel.text = @"MMAnimationCurveCircularIn";
        }
            break;
        case MMAnimationCurveCircularOut: {
            cell.textLabel.text = @"MMAnimationCurveCircularOut";
        }
            break;
        case MMAnimationCurveCircularInOut: {
            cell.textLabel.text = @"MMAnimationCurveCircularInOut";
        }
            break;
        case MMAnimationCurveBackIn: {
            cell.textLabel.text = @"MMAnimationCurveBackIn";
        }
            break;
        case MMAnimationCurveBackOut: {
            cell.textLabel.text = @"MMAnimationCurveCircularOut";
        }
            break;
        case MMAnimationCurveBackInOut: {
            cell.textLabel.text = @"MMAnimationCurveBackInOut";
        }
            break;
    }
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _listOfAnimationCurves.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.row) {
        case MMAnimationCurveAppleIn: {
            _beatBallsView.animationCurve = MMAnimationCurveTimeBlockAppleIn;
        }
            break;
        case MMAnimationCurveAppleOut: {
            _beatBallsView.animationCurve = MMAnimationCurveTimeBlockAppleOut;
        }
            break;
        case MMAnimationCurveAppleInOut: {
            _beatBallsView.animationCurve = MMAnimationCurveTimeBlockAppleInOut;
        }
            break;
        case MMAnimationCurveQuadraticIn: {
            _beatBallsView.animationCurve = MMAnimationCurveTimeBlockQuadraticIn;
        }
            break;
        case MMAnimationCurveQuadraticOut: {
            _beatBallsView.animationCurve = MMAnimationCurveTimeBlockQuadraticOut;
        }
            break;
        case MMAnimationCurveQuadraticInOut: {
            _beatBallsView.animationCurve = MMAnimationCurveTimeBlockQuadraticInOut;
        }
            break;
        case MMAnimationCurveExpoIn: {
            _beatBallsView.animationCurve = MMAnimationCurveTimeBlockExpoIn;
        }
            break;
        case MMAnimationCurveExpoOut: {
            _beatBallsView.animationCurve = MMAnimationCurveTimeBlockExpoOut;
        }
            break;
        case MMAnimationCurveExpoInOut: {
            _beatBallsView.animationCurve = MMAnimationCurveTimeBlockExpoInOut;
        }
            break;
        case MMAnimationCurveCircularIn: {
            _beatBallsView.animationCurve = MMAnimationCurveTimeBlockCircularIn;
        }
            break;
        case MMAnimationCurveCircularOut: {
            _beatBallsView.animationCurve = MMAnimationCurveTimeBlockCircularOut;
        }
            break;
        case MMAnimationCurveCircularInOut: {
            _beatBallsView.animationCurve = MMAnimationCurveTimeBlockCircularInOut;
        }
            break;
        case MMAnimationCurveBackIn: {
            _beatBallsView.animationCurve = MMAnimationCurveTimeBlockBackIn;
        }
            break;
        case MMAnimationCurveBackOut: {
            _beatBallsView.animationCurve = MMAnimationCurveTimeBlockBackOut;
        }
            break;
        case MMAnimationCurveBackInOut: {
            _beatBallsView.animationCurve = MMAnimationCurveTimeBlockBackInOut;
        }
            break;
    }
    
    [self _showBallsAnimation];
}

#pragma mark - Helpers

- (void)_showBallsAnimation {
    for (int i = 0; i < _beatBallsView.numberOfBalls; i++) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)((float)i*0.25f*NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [_beatBallsView showMovementAnimationForBallAtIndex:i];
        });
    }
}

@end
