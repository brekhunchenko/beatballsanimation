//
//  MMBeatBallsAnimationCurves.h
//  BeatBallsAnimation
//
//  Created by Yaroslav on 8/30/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

typedef double (^MMAnimationCurveTimeBlock)(double time);

extern const MMAnimationCurveTimeBlock MMAnimationCurveTimeBlockLinear;

/* Apple */
extern const MMAnimationCurveTimeBlock MMAnimationCurveTimeBlockAppleIn;
extern const MMAnimationCurveTimeBlock MMAnimationCurveTimeBlockAppleOut;
extern const MMAnimationCurveTimeBlock MMAnimationCurveTimeBlockAppleInOut;

/* Quadratic */
extern const MMAnimationCurveTimeBlock MMAnimationCurveTimeBlockQuadraticIn;
extern const MMAnimationCurveTimeBlock MMAnimationCurveTimeBlockQuadraticOut;
extern const MMAnimationCurveTimeBlock MMAnimationCurveTimeBlockQuadraticInOut;

/* Expo */
extern const MMAnimationCurveTimeBlock MMAnimationCurveTimeBlockExpoIn;
extern const MMAnimationCurveTimeBlock MMAnimationCurveTimeBlockExpoOut;
extern const MMAnimationCurveTimeBlock MMAnimationCurveTimeBlockExpoInOut;

/* Circular */
extern const MMAnimationCurveTimeBlock MMAnimationCurveTimeBlockCircularIn;
extern const MMAnimationCurveTimeBlock MMAnimationCurveTimeBlockCircularOut;
extern const MMAnimationCurveTimeBlock MMAnimationCurveTimeBlockCircularInOut;

/* Back */
extern const MMAnimationCurveTimeBlock MMAnimationCurveTimeBlockBackIn;
extern const MMAnimationCurveTimeBlock MMAnimationCurveTimeBlockBackOut;
extern const MMAnimationCurveTimeBlock MMAnimationCurveTimeBlockBackInOut;

extern const double (^MMAnimationCurveAnimationBezierEvaluator)(double time, CGPoint ct1, CGPoint ct2);
