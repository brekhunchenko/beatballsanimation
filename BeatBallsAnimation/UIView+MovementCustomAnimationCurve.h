//
//  UIView+MovementCustomAnimationCurve.h
//  BeatBallsAnimation
//
//  Created by Yaroslav on 8/30/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMBeatBallsAnimationCurves.h"

@interface UIView (MovementCustomAnimationCurve)

+ (void)animateMovementFrom:(CGPoint)p0
                         to:(CGPoint)p1
                       view:(UIView *)view
             animationCurve:(MMAnimationCurveTimeBlock)animationCurve
                   duration:(NSTimeInterval)duration
                 completion:(void (^)(BOOL finished))completionBlock;

@end
